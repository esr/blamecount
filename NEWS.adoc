= blamecount project news.

1.1: 2024-02-07::
   Added -a and -c options to report commit commit authorship statistics.

1.0: 2024-01-08::
   Initial release.
